<?php

namespace Application\ExchangeRate;

use Application\Exception\CurrencyPairNotSupported;
use Money\Currency;

class RandomExchangeRate implements ExchangeRateProvider
{
    public function fetch(Currency $currencyIn, Currency $currencyOut)
    {
        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'PLN') {
            return round(4 + rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'GBP' && $currencyOut->getCode() === 'EUR') {
            return round(1 + rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'EUR') {
            return round(rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'PLN' && $currencyOut->getCode() === 'GBP') {
            return round(rand() / getrandmax(), 4);
        }

        if ($currencyIn->getCode() === 'EUR' && $currencyOut->getCode() === 'PLN') {
            return round(4 + rand() / getrandmax(), 4);
        }

        throw new CurrencyPairNotSupported();
    }
}
